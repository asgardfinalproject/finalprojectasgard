### Test Case U_25


# Title : 
Registered user dislike a comment

# Priority: Prio 1

# Test Case Description: 
As a registered user I would like to be able to dislike a comment

# Pre-Conditions: 
User logged in with email (bpopova@abv.bg) and password (Hello@2020)

# Test Steps: 

1. Navigate to https://intense-mesa-78449.herokuapp.com choose LATEST POSTS dropdown menu from navigation bar and choose ALL POSTS
2. Click on the name of one the posts
3. Go to Comments field
3. Go to Like button under the comment and click on it



# Expected Result: 
The figure under the comment is increased by one

