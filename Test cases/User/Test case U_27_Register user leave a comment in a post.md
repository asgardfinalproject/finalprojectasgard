
### Test Case U_27

# Title : 
Register user leave a comment in a post

# Priority: Prio 1

# Test Case Description: 
As a registered user, I would like to be able to comment on a post

# Pre-Conditions:
User is logged in

# Test steps:

1. Navigate to https://intense-mesa-78449.herokuapp.com and from navigation bar click on LATEST POSTS dropdown menu and choose ALL POSTS
2. Click on one of posts' title
3. Go to Leave a Comment section
4. Click on the input field and type your comment
5. Click on SEND button

# Expected Result: 
The comment is available in Comments section


