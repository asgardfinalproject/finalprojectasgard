### Test Case U_17

# Title : 
Unsuccessful creating a new post 

# Priority: Prio 1

# Test Case Description: 
As a registered user I would like to be able to create a new post 

# Pre-Conditions: 
User logged in with email (bpopova@abv.bg) and password (Hello@2020)

# Test Steps: 

1. Navigate to https://intense-mesa-78449.herokuapp.com and from navigation bar click on POSTS dropdown menu and choose NEW post
2. Click on and choose PUBLIC or CONNECTION from Post visibility dropdown menu
3. Go to Post picture or video field, Click on Choose File button and choose a video or picture and upload it
4. Click on and type title under 2 and up to 100 symbols on Title placeholder
5. Click on SAVE button


# Expected Result: 
A Message appears which seys "Post's title should be between 2 and 100 characters." 

