### Test Case U_19 (ISSUE #)

# Title : 
Registered user edit own post

# Priority: Prio 1

# Test Case Description: 
As a registered user I would like to be able to edit post

# Pre-Conditions: 
User logged in with email (bpopova@abv.bg) and password (Hello@2020)

# Test Steps: 

1. Navigate to https://intense-mesa-78449.herokuapp.com and from navigation bar click on PROFILE dropdown menu and choose PROFILE
2. Click on one of my posts' title
3. Click on EDIT link
4. Change the description
5. Click on SAVE button


# Expected Result: 
Post is updated
 