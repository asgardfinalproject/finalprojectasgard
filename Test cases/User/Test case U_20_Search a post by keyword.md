### Test Case U_20

# Title : 
User search a post

# Priority: Prio 2

# Test Case Description: 
As a registered user I would like to be able to find a post by keyword

# Pre-Conditions: 
User logged in with email (bpopova@abv.bg) and password (Hello@2020)

# Test Steps:  

1. Navigate to https://intense-mesa-78449.herokuapp.com and from navigation bar click on LATEST POSTS dropdown menu and choose ALL POSTS
2. See all public users posts
3. Go to the Serach Field and type a keyword
4. Click on the SEARCH button


# Expected Result: 
User found a post by keyword

