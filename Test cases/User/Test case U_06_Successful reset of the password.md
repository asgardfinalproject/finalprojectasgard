### Test Case U_06

# Title : 
Successful reset the password

# Priority: Prio 1

# Test Case Description: 
As a user I would like to be able to reset of my password

# Pre-Conditions: 
User logged in with email (bpopova@abv.bg) and password (Hello@2020)

# Test Steps: 

1. Navigate to https://intense-mesa-78449.herokuapp.com and from navigation bar click on SIGN IN
2. Click on and type wrong email in the username field and correct password in the password field
3. Click on and type wrong password in the password field and correct email in the email field
4. Click on LOGIN button
5. Go to and click on "Forgot password" link
6. Click on and type your valid email
7. Click on SEND button
8. Go to your email inbox and click on Reset password confirmation link
9. Go to the page to fill and confirm a new password
10. Click on SAVE button
11. Go to Login page and fill correct email and new password
12. Click LOGIN button


# Expected Result: 
User successfully reset of the password, has been logged in with the new one and is able to see home page with navigation bar dropdown menu
