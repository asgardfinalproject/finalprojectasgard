### Test Case U_35


# Title : 
Request to connect with other user and its confirmation by the other user

# Priority: Prio 1

# Test Case Description: 
As a registered user, I would like to be able to send request for connection to other user and to connect with him/her after confirmation from his/her part

# Pre-Conditions: 
User logged in with email (bpopova@abv.bg) and password (Hello@2020)

# Test Steps: 

1. Navigate to https://intense-mesa-78449.herokuapp.com and from navigation bar click on USERS dropdown menu and choose USERS
2. See all public users profiles
3. Click on relevant user's name
4. Click on CONNECT button
5. Click on Logout button
6. Click on Sign in button from the Navigation bar
7. Fill in email and password in order to login in the web application
8. Click on USERS from Navigation bar and choose REQUESTS
9. Click on the user's name who send the request
10.Click on CONFIRM button


# Expected Result: 
SENT REQUEST (REJECT) button appears in the user's profile who is sending the request.
Yellow bell icon appears next to REQUESTS from USERS drop down menu. After confirmation it desappears and a button Disconnect appears.

