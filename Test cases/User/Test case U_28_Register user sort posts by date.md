
### Test Case U_28


# Title : 
Register user sort posts by date

# Priority: Prio 3

# Test Case Description: 
As a registered user, I would like to be able to sort any post by date

# Pre-Conditions:
User is logged in

# Test steps:

1. Navigate to https://intense-mesa-78449.herokuapp.com and from navigation bar click on LATEST POSTS dropdown menu and choose ALL POSTS
2. Go to the Sort section and click on Date button
 

# Expected Result: 
The posts have been sorted by date. The most resent post appears on the top of the list