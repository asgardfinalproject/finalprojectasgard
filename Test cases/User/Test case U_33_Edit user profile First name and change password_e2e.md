### Test Case U_34

# Title : 
Registered user edit profile First name and password

# Priority: Prio 1

# Test Case Description: 
As a registered user I would like to be able to edit my first name on my profile and to change my password

# Pre-Conditions: 
User logged in with email (sofiya.georgieva@abv.bg) and password (Univercity1@)

# Test Steps: 

1. Navigate to https://intense-mesa-78449.herokuapp.com and from navigation bar click on PROFILE and choose EDIT
2. Click on First name section and type new first name
3. Click on SAVE button
4. Click on Edit button
5. Click on Change password button
6. Click on enter old password input field and type your old password
7. Click on enter password input field and type your new password
8. Click on enter confirm password input field and type your new password again
9. Click on SAVE button


# Expected Result: 
User's first name has been updated and it appears on the user's profile. The password is now changed.

 