
### Test Case ID U_01

# Title : 
Register user filling required fields and create user profile 

# Priority: Prio 1

# Test Case Description: 
As an anonymus user, I would like to be able to register new user filling required fields

# Pre-Conditions:
Created new valid e-mail

# Test Steps: 

1. Navigate to https://intense-mesa-78449.herokuapp.com and from navigation bar click on SIGN UP
2. Click on and type the email in the email field
3. Click on and type password in the password field 
4. Click on and type the passoword in confirm password field
5. Click on and type user's first name in First Name field
6. Click on and type user's last name in Last Name field
7. Click on Profile Picture Visibility and choose one option from the dropdown menu
8. Click on Choose file button under Profile Picture field
9. Choose new picture from PC and click Open button to upload it
10. Click on REGISTER button
11. Go to your email, open Email Confirmation from the inbox
12. Click on the link in the email in order to confirm your registration
 
# Test Data:
username (email) = eli.dulevska@abv.bg
password = Hello@2020
First Name = Eli
Last Name = Beli

# Expected Result: 
LOGIN page appears with messages for Successful registration and message to active your account by follow link sent to the email address


