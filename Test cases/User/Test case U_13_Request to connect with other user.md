### Test Case U_13


# Title : 
Request to connect with other user

# Priority: Prio 1

# Test Case Description: 
As a registered user I would like to be able to connect with other user

# Pre-Conditions: 
User logged in with email (bpopova@abv.bg) and password (Hello@2020)

# Test Steps: 

1. Navigate to https://intense-mesa-78449.herokuapp.com and from navigation bar click on USERS dropdown menu and choose USERS
2. See all public users profiles
3. Click on relevant user's name
4. Click on CONNECT button


# Expected Result: 
On profile user page SENT REQUEST (REJECT) button appears

