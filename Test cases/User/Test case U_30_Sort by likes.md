### Test Case U_30

# Title : 
Registered user can sort posts by likes

# Priority: Prio 3

# Test Case Description: 
As a registered user, I would like to be able to sort the posts, based on how many likes they have

# Pre-Conditions:
User is logged in

# Tests steps: 

1. Navigate to https://intense-mesa-78449.herokuapp.com and from the navigation bar select LATEST POSTS button
2. Go to the Sort section and click on Likes button


# Expected Result: 
The posts have been sorted by number of likes. The post with the biggest number of likes appears on the top of the list