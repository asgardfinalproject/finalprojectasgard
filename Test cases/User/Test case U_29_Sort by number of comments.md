### Test Case U_29


# Title : 
Registered user can sort posts by comments

# Priority: Prio 3

# Test Case Description: 
As a regitered user, I would like to be able to sort posts by comments

# Pre-Conditions:
User is logged in

# Test steps: 

1. Navigate to https://intense-mesa-78449.herokuapp.com and from the navigation bar select LATEST POSTS button
2. Go to the Sort section and click on Comments button


# Expected Result: 
The posts have been sorted by number of comments. The post with the biggest number of comments appears on the top of the list