### Test Case U_23

# Title : 
Registered user like a post

# Priority: Prio 1

# Test Case Description: 
As a registered user I would like to be able to like a post

# Pre-Conditions: 
User logged in with email (bpopova@abv.bg) and password (Hello@2020)

# Test Steps: 

1. Navigate to https://intense-mesa-78449.herokuapp.com choose LATEST POSTS dropdown menu from navigation bar and choose ALL POSTS
2. Click on the name of one the posts
3. Go to Like button under the post and click on it



# Expected Result: 
The figure under the post is increased by one

