### Test Case U_15


# Title : 
Registered user create new public post and make comment

# Priority: Prio 1

# Test Case Description: 
As a registered user I would like to be able to create a new public post and make comment

# Pre-Conditions: 
User logged in with email (sofiya.georgieva@abv.bg) and password (Univercity1@)

# Test Steps:  

1. Navigate to https://intense-mesa-78449.herokuapp.com and from navigation bar click on LATEST POSTS dropdown menu and choose NEW post
2. Click on and choose Public from Post visibility dropdown menu
3. Click on and type title between 2 and 100 symbols on Title field
4. Go to Post picture or video section, Click on Choose File button, choose new picture from PC and click Open button to upload it
5. Click on SAVE button
6. From navigation bar click on LATEST POSTS dropdown menu and choose ALL POSTS
7. Click on the name of a selected post
8. Go to Leave a Comment placeholder and type text
9. Click on SAVE button


# Expected Result: 
A new post has been created and visible on the user's feed. New comment has been created and visible on the Comments post's section.



