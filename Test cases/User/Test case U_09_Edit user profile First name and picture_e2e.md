### Test Case U_09

# Title : 
Registered user edit profile First name and picture

# Priority: Prio 1

# Test Case Description: 
As a registered user I would like to be able to edit my profile First name and picture

# Pre-Conditions: 
User logged in with email (sofiya.georgieva@abv.bg) and password (Univercity1@)

# Test Steps: 

1. Navigate to https://intense-mesa-78449.herokuapp.com and from navigation bar click on PROFILE and choose EDIT
2. Click on First name section and type new first name
3. Click on Choose File button from Profile picture section 
4. Choose new picture from PC and click Open button to upload it
5. Click on SAVE button



# Expected Result: 
User is Able to see profile page and First name and profile picture has been changed
 