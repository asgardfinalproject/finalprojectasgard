
### Test Case ID U_03

# Title : 
Unsuccessful user registration 

# Priority: Prio 1

# Test Case Description: 
As an anonymus user, I would like to be able to register new user and create profile

# Pre-Conditions:
Created new valid e-mail

# Test Steps: 

1. Navigate to https://intense-mesa-78449.herokuapp.com and from navigation bar click on SIGN UP
2. Click on and type username or invalid email in the email field
3. Click on and type password in the password field 
4. Click on and type the passoword in confirm password field
5. Click on and type user's first name in First Name field
6. Click on and type user's last name in Last Name field
7. Click on Profile Picture Visibility and choose one option from the dropdown menu
8. Click on Choose file button under Profile Picture field
9. Choose new picture from PC and click Open button to upload it
10. Click on REGISTER button

 
# Test Data:
username (email) = Eli
password = Hello@2020
First Name = Eli
Last Name = Beli

# Expected Result: 
A message appears "Not valid email format"


