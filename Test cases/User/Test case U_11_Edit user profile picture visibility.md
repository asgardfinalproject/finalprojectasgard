### Test Case U_11

# Title : 
Registered user edit profile picture

# Priority: Prio 1

# Test Case Description: 
As a registered user I would like to be able to edit my profile picture

# Pre-Conditions: 
User logged in with email (bpopova@abv.bg) and password (Hello@2020)

# Test Steps: 

1. Navigate to https://intense-mesa-78449.herokuapp.com and from navigation bar click on PROFILE and choose EDIT
2. Click on Profile picture visibility section 
3. Choose connections if public visibility is selected
4. Choose public if connections visibility is selected
5. Click on SAVE button



# Expected Result: 
Picture visibility is updated
 