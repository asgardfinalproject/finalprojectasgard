### Test Case U_07

# Title : 
Registered user change password

# Priority: Prio 1

# Test Case Description: 
As a registered user I would like to be able to change my password

# Pre-Conditions: 
User logged in with email (bpopova@abv.bg) and password (Hello@2020)

# Test Steps: 

1. Navigate to https://intense-mesa-78449.herokuapp.com and from navigation bar click on PROFILE and choose EDIT
2. Click on EDIT button
3. Click on CHANGE PASSWORD button
4. Go to the resetpassword page
5. Click on and type old password in the old password field
6. Click on and type new password in the enter password field
7. Click on and type new password in the confirm password field
8. Click on SAVE button


# Expected Result: 
User successfully change password and is able to see home page with navigation bar dropdown menu

 