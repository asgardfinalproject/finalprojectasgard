### Test Case U_16

# Title : 
Registered user create new connections post with video filling required sections

# Priority: Prio 1

# Test Case Description: 
As a registered user I would like to be able to create a new post with video filling required sections

# Pre-Conditions: 
User logged in with email (bpopova@abv.bg) and password (Hello@2020)

# Test Steps:  

1. Navigate to https://intense-mesa-78449.herokuapp.com and from navigation bar click on LATEST POSTS dropdown menu and choose NEW post
2. Click on and choose CONNECTIONS from Post visibility dropdown menu
3. Click on and type title between 2 and 100 symbols on Title field
4. Click on SAVE button
5. From navigation bar click on LATEST POSTS dropdown menu and choose ALL POSTS


# Expected Result: 
A new connections post has been created and visible for user, but is not visible for other users

