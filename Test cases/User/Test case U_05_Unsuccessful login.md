### Test Case U_05

# Title : 
Unsuccessful User login

# Priority: Prio 1

# Test Case Description: 
As a user, I would like to be able to login


# Pre-Conditions: 
User logged in with email (bpopova@abv.bg) and password (Hello@2020)

# Test Steps:  

1. Navigate to https://intense-mesa-78449.herokuapp.com and from navigation bar click on SIGN IN
2. Click on and type wrong email in the username field and correct password in the password field
3. Click on and type wrong password in the password field and correct email in the email field
4. Click on LOGIN button


# Expected Result: 
On Login page is appeared message "Wrong username or password" 

