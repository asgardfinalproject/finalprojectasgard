### Test Case U_14

# Title : 
User approve request for connection sent by another user

# Priority: Prio 1

# Test Case Description: 
As a registered user I would like to be able to approve request to connection sent by another user

# Pre-Conditions: 
User logged in with email (bpopova@abv.bg) and password (Hello@2020)

# Test Steps:  

1. Navigate to https://intense-mesa-78449.herokuapp.com and from navigation bar click on USERS dropdown menu and choose REQUESTS
2. Click on REQUEST button
3. Click on relevant user's name
4. Click on CONFIRM button


# Expected Result: 
On profile user page DISCONNECT button appears

