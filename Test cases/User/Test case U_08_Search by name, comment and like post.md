### Test Case U_08

# Title : 
User search a profile by name/keyword, make comments and like a psot

# Priority: Prio 2

# Test Case Description: 
As a registered user I would like to be able to find a specific profile, make comment and like a post

# Pre-Conditions: 
User logged in with email (bpopova@abv.bg) and password (Hello@2020)

# Test Steps: 

1. Navigate to https://intense-mesa-78449.herokuapp.com choose USERS dropdown menu from navigation bar and choose USERS
2. Go to the placeholder Search Keyword and type an user name
3. Click on the SEARCH button
4. Click on the name of found user profile
5. Click on one of the posts and leave a comment in comment field
6. Click on SEND button
7. Click on Like button under post title


# Expected Result: 
User is found, the new comment is appeared in Comments field, the Like figure is increased by one

