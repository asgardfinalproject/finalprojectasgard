### Test Case U_31


# Title : 
Registered user create new public post, update, like and delete it

# Priority: Prio 1

# Test Case Description: 
As a registered user I would like to be able to create a new public post, update, like and delete it

# Pre-Conditions: 
User logged in with email (asgardalphaqa@gmail.com) and password (Test1Asgard@)

# Test Steps:  

1. Navigate to https://intense-mesa-78449.herokuapp.com and from navigation bar click on LATEST POSTS dropdown menu and choose NEW post
2. Click on and type title between 2 and 100 symbols on Title field
3. Go to Post picture or video section, Click on Choose File button, choose new picture from PC and click Open button to upload it
4. Click on SAVE button
5. From navigation bar click on LATEST POSTS dropdown menu and choose ALL POSTS
6. Click on the name of a selected post
7. Go to Leave a Comment placeholder and type text
8. Click on SAVE button
9. Go to Home page and from Navigation bar click on PROFILE dropdown menu and choose PROFILE
10. Click on the my new posts' title
11. Click on EDIT link
12. Change the description
13. Click on SAVE button
14. Go to Home page and from Navigation bar click on PROFILE dropdown menu and choose PROFILE
15. Click on the my new posts' title
16. Click on LIKE link
17. Click on EDIT link
18. Click on DELETE button


# Expected Result: 
A new post has been created and visible on the user's feed. New post has been updated. Post has been liked and the number of likes is equal to one. The post has been deleted and no longer available. 



