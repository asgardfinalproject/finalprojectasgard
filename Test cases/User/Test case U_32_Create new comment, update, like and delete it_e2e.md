### Test Case U_32


# Title : 
Registered user create new comment, update it, like and delete it

# Priority: Prio 1

# Test Case Description: 
As a registered user I would like to be able to create a new comment, update it, like it and delete it

# Pre-Conditions: 
User logged in with email (asgardalphaqa@gmail.com) and password (Test1Asgard@)

# Test Steps:  

1. Navigate to https://intense-mesa-78449.herokuapp.com and from navigation bar click on LATEST POSTS dropdown menu and choose a post
2. Click on the name of a selected post
3. Go to Leave a Comment placeholder and type text
4. Click on SAVE button
5. Go to Comments section and click on Edit link
6. Update comment in comment's placeholder
7. Click on SAVE button
8. Go to Comments section and click on Like link
9. Go to Comments section and click on Edit link
10. Click on DELETE button



# Expected Result: 
A new comment has been created and visible on the post's feed. New comment has been updated. Comment has been liked and the number of likes is equal to one. The comment has been deleted and no longer available. 



