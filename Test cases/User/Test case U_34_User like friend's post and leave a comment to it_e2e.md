### Test Case U_34

# Title : 
Registered user edit profile First name and password

# Priority: Prio 1

# Test Case Description: 
As a registered user, I would like to be able to choose my friend's post, like it and leave a comment to it

# Pre-Conditions: 
User logged in with email (sofiya.georgieva@abv.bg) and password (Univercity1@)

# Test Steps: 

1. Navigate to https://intense-mesa-78449.herokuapp.com and from navigation bar click on USERS and click on FRIENDS
2. Click on one friend's name
3. Click on one of his/her posts and click on Like button
4. Go to Leave a comment section on the right and type your comment in the text box
5. Click on SEND button

# Expected Result: 
User is able to enter one friend's post and see its content. The post is liked - the heart emoticon under the post is increased by one and button dislike appears.
The new comment is available in the comment section.
 