### Test Case U_23

# Title : 
Created new post is chronologically ordered in the all post's feed

# Priority: Prio 2

# Test Case Description: 
As a registered user I would like to be able to create a new post and it should be chronologically ordered in the all post's feed

# Pre-Conditions: 
User logged in with email (bpopova@abv.bg) and password (Hello@2020)

# Test Steps:  

1. Navigate to https://intense-mesa-78449.herokuapp.com and from navigation bar click on LATEST POSTS dropdown menu and choose NEW post
2. Click on and choose PUBLIC from Post visibility dropdown menu
3. Click on and type title between 2 and 100 symbols on Title field
4. Click on SAVE button
5. From navigation bar click on LATEST POSTS dropdown menu and choose ALL POSTS



# Expected Result: 
The last created post is at the top of the all post's feed

