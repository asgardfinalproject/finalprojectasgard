### Test Case U_21

# Title : 
User reject request for connection sent by another user

# Priority: Prio 2

# Test Case Description: 
As a registered user I would like to be able to reject request to connect sent by another user 

# Pre-Conditions: 
User logged in with email (bpopova@abv.bg) and password (Hello@2020)

# Test Steps:  

1. Navigate to https://intense-mesa-78449.herokuapp.com and from navigation bar click on USERS dropdown menu and choose REQUESTS
2. Click on REQUEST button
3. Click on relevant user's name
4. Click on REJECT button


# Expected Result: 
On profile user page CONNECT button appears

