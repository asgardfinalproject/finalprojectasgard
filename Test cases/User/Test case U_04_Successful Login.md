### Test Case U_04

# Title : 
Successful User login

# Priority: Prio 1

# Test Case Description: 
As a user,  I would like to be able to login


# Pre-Conditions: 
User logged in with email (bpopova@abv.bg) and password (Hello@2020)

# Test Steps: 

1. Navigate to https://intense-mesa-78449.herokuapp.com and from navigation bar click on SIGN IN
2. Click on and type the email in the username field
3. Click on and type password in the password field
4. Click on LOGIN button


# Expected Result: 
User has been logged in and is able to see home page with navigation bar dropdown menu

