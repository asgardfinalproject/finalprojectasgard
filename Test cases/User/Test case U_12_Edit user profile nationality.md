### Test Case U_12


# Title : 
Registered user edit nationality

# Priority: Prio 2

# Test Case Description: 
As a registered user I would like to be able to edit my nationality

# Pre-Conditions: 
User logged in with email (bpopova@abv.bg) and password (Hello@2020)

# Test Steps: 

1. Navigate to https://intense-mesa-78449.herokuapp.com and from navigation bar click on PROFILE and choose EDIT
2. Click on Nationality section 
3. Choose from dropdawn menu other nationality
4. Click on SAVE button



# Expected Result: 
User is Able to see profile page and Nationality is changed
 