### Test Case An_06

# Title : 
Anonymous user can sort posts by category

# Priority: Prio 3

# Test Case Description: 
Anonymous user can see all public posts and can sort them by category - positive test case

# Pre-Conditions:

# Test Steps: 

1. Open the application home page and on landing page select LATEST POSTS button
2. See the feed with public posts
3. Go to the Category section and click on relevant category


# Test Data: 

# Expected Result: 
The anonymus user is able to see all public posts, choose relevant category from Category list and see all posts from the choosen category.