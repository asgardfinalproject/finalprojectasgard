### Test Case An_07

# Title : 
An anonymous user can search another user

# Priority: Prio 2

# Test Case Description: 
As an anonymous user, I would like to be able to search another user by name

# Pre-Conditions:

# Test Steps: 

1. Navigate to https://intense-mesa-78449.herokuapp.com and from the navigation bar select USERS button
2. Go to Search field and type the name of the user you are looking for
3. Click on SEARCH button
4. Click on the name of the user



# Expected Result: 
The searched user appears on the page