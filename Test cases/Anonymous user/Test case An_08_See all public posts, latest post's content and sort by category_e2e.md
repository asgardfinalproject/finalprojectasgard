### Test Case An_08

# Title : 
An anonymous user see HomePage post, sort by categories, see User profile 

# Priority: Prio 1

# Test Case Description: 
As an anonymous user, I would like to be able to see a post from HomePage and his/her creator's profile and his/her latest post. To be able to see all users, choose one of them and see his/her profile. To be able to select the posts by category and choose one of the posts.

# Preconditions:

# Test Steps: 

1. Navigate to https://intense-mesa-78449.herokuapp.com and from the navigation bar select LATEST POSTS button
2. Choose a category from Category section
3. Select a post from choosen category
4. Click on USERS button from navigation page
5. Choose on the users
6. Click on his/her profile
7. From Home Page choose first post
8. Click on his/her creator's name
9. Choose latest his/her post from profile



# Expected Result: 
See the choosen post from current category. User's profile is opened. See the first post from Home Page, see his/her creator's profile and his/her latest post. 

