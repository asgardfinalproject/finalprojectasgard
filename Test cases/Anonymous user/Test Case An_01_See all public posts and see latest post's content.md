
### Test Case An_01

# Title : 
An anonymous user see all public posts and see latest post's content

# Priority: Prio 1

# Test Case Description: 
As an anonymous user, I would like to be able to see all public posts, to see the content of the most recent one and to sort by category

# Preconditions:

# Test Steps: 

1. Navigate to https://intense-mesa-78449.herokuapp.com and from the navigation bar select LATEST POSTS button
2. Click on the title of any post
4. Enter the post's content


# Expected Result: 
The content of the selected post is visible to the anonymous user

