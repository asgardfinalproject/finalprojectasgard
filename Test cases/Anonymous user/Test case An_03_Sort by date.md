### Test Case An_03

# Title : 
An anonymous user can sort posts by date

# Priority: Prio 3

# Test Case Description: 
As an anonymous user, I would like to be able to sort posts by date

# Pre-Conditions:

# Test Steps:  

1. Navigate to https://intense-mesa-78449.herokuapp.com and from the navigation bar select LATEST POSTS button
2. Go to the Sort section and click on Date button
 

# Expected Result: 
The posts have been sorted by date. The most resent post appears on the top of the list