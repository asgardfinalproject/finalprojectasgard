### Test Case An_02

# Title : 
An anonymous user can search post by keyword

# Priority: Prio 2

# Test Case Description: 
As an anonymous user, I would like to be able to search a post by keyword

# Pre-Conditions:

# Test Steps: 

. Navigate to https://intense-mesa-78449.herokuapp.com and from the navigation bar select LATEST POSTS button
2. See the feed with public posts
3. Go to the Serach Field and type a keyword
4. Click on the SEARCH button


# Test Data: 


# Expected Result: 
The anonymus user is able to see all public posts searched by keyword

