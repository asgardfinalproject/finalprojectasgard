### Test Case Admin_12

# Title : 
Registered system administrator add a new nationality

# Priority: Prio 1

# Test Case Description: 
As a registered system administrator, I would like to be able to add a new nationality

# Pre-Conditions: 
System administrator logged in with email (asgardalphaqa@gmail.com) and password (Test1Asgard@)

# Test Steps: 

1. Navigate to https://intense-mesa-78449.herokuapp.com and from navigation bar click on SETTINGS dropdown menu and choose NATIONALITIES
2. Click on Add button
3. Click on Nationality's name field and type a new nationality
4. Click on SAVE button

# Expected Result: 
The new nationality is now visible on the bottom of the list of all nationalities

 