### Test Case Admin_14

# Title : 
Registered system administrator delete nationality

# Priority: Prio 2

# Test Case Description: 
As a registered system administrator, I would like to be able to delete nationality

# Pre-Conditions: 
System administrator logged in with email (asgardalphaqa@gmail.com) and password (Test1Asgard@)

# Test Steps:  

1. Navigate to https://intense-mesa-78449.herokuapp.com and from navigation bar click on SETTINGS dropdown menu and choose NATIONALITIES
2. Click on Edit button of the choosen nationality
3. Click on DELETE button
4. Go to page with message "Are you sure you want to delete nationality" and current nationality
5. Click on Yes button

# Expected Result: 
The selected nationality is no longer available on the list of all nationalities
 