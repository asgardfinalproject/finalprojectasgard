### Test Case Admin_07

# Title : 
Registered system administrator delete post's comment

# Priority: Prio 1

# Test Case Description: 
As a registered system administrator, I would like to be able to delete post's comment

# Pre-Conditions: 
System administrator logged in with email (asgardalphaqa@gmail.com) and password (Test1Asgard@)

# Test Steps: 

1. Navigate to https://intense-mesa-78449.herokuapp.com and from navigation bar click on SETTINGS dropdown menu and choose POSTS
2. Click on one of posts' title
3. Go to Comments section
4. Click on Edit link
5. Click on DELETE button
6. Go to page with message "Are you sure you want to delete comment" and current comment
7. Click on Yes button

# Expected Result: 
The comment is not visible anymore in the comments' list for this post
 