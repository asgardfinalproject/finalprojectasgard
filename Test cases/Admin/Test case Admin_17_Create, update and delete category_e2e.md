### Test Case Admin_17

# Title : 
Registered system administrator create, update and delete category

# Priority: Prio 1

# Test Case Description: 
As a registered system administrator, I would like to be able to create, update and delete category to the list of all categories

# Pre-Conditions: 
System administrator logged in with email (asgardalphaqa@gmail.com) and password (Test1Asgard@)

# Test Steps: 

1. Navigate to https://intense-mesa-78449.herokuapp.com and from navigation bar click on SETTINGS dropdown menu and choose CATEGORIES
2. Click on Add button
3. Click on Choose file from Categoriy's emoticon
4. Choose a new picture from PC and click Open button to upload it
5. Click on input field and type new category name from Categoy's name section
6. Click on SAVE button
7. Select the category you want to update and click on Edit button
8. Click on category's name input field and type new name
9. Click on Categoy's emoticon section and click on Choose file button
10. Choose new picture from PC and click Open button to upload it.
11. Click on SAVE button
12. Select the category you want to delete and click on Edit button
13. Click on DELETE button
14. Go to page with message "Are you sure you want to delete category" and current category
15. Click on Yes button



# Expected Result: 
New category has been created, updated and deleted

 