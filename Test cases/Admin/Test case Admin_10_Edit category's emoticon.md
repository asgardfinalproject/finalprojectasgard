### Test Case Admin_10

# Title : 
Registered system administrator edit category's emoticon

# Priority: Prio 2

# Test Case Description: 
As a registered system administrator, I would like to be able to edit category's emoticon

# Pre-Conditions: 
System administrator logged in with email (asgardalphaqa@gmail.com) and password (Test1Asgard@)

# Test Steps:  

1. Navigate to https://intense-mesa-78449.herokuapp.com and from navigation bar click on SETTINGS dropdown menu and choose CATEGORIES
2. Select the category you want to update and click on Edit button
3. Click on Categoy's emoticon section and click on Choose file button
4. Choose new picture from PC and click Open button to upload it.
5. Click on SAVE button

# Expected Result: 
The new category's emoticon is now visible on the list of all categories
 