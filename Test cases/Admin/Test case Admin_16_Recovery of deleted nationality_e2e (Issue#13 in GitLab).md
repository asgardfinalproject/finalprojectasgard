### Test Case Admin_16

# Title : 
Registered system administrator recover deleted nationality

# Priority: Prio 2

# Test Case Description: 
As a registered system administrator, I would like to be able to recover deleted nationality

# Pre-Conditions: 
System administrator logged in with email (asgardalphaqa@gmail.com) and password (Test1Asgard@)

# Test Steps:  

1. Navigate to https://intense-mesa-78449.herokuapp.com and from navigation bar click on SETTINGS dropdown menu and choose NATIONALITIES
2. Click on Add button
3. Click on Nationality's name field and type a name of deleted nationality
4. Click on SAVE button

# Expected Result: 
The recovered nationality is visible in the list of all nationalities

