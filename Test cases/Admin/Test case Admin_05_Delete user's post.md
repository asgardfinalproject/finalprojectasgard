### Test Case Admin_05

# Title : 
Registered system administrator delete user's post

# Priority: Prio 1

# Test Case Description: 
As a registered system administrator, I would like to be able to delete user's post

# Pre-Conditions: 
System administrator logged in with email (asgardalphaqa@gmail.com) and password (Test1Asgard@)

# Test Steps:  

1. Navigate to https://intense-mesa-78449.herokuapp.com and from navigation bar click on USERS dropdown menu and choose USERS
2. Click on the relevant user's name
3. Click on the relevant post's title
3. Click on Edit link
4. Click on DELETE button
5. Go to page with message "Are you sure you want to delete post" and post's title
6. Click on Yes button

# Expected Result: 
The post is not visible anymore in posts' list