### Test Case Admin_02

# Title : 
Registered system administrator edit user's profile picture

# Priority: Prio 1

# Test Case Description: 
As a registered system administrator, I would like to be able to edit user's profile picture

# Pre-Conditions: 
System administrator logged in with email (asgardalphaqa@gmail.com) and password (Test1Asgard@)

# Test Steps: 

1. Navigate to https://intense-mesa-78449.herokuapp.com and from navigation bar click on USERS dropdown menu and choose USERS
2. Click on the relevant user's name
3. Click on Edit button
4. Click on Choose File button from Profile picture section 
5. Choose new picture from PC and click Open button to upload it
6. Click on SAVE button

# Expected Result: 
User's picture has been updated. The new picture appears on the screen
 
 