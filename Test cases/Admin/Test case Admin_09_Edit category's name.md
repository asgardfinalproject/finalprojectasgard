### Test Case Admin_09

# Title : 
Registered system administrator edit category's name

# Priority: Prio 2

# Test Case Description: 
As a registered system administrator, I would like to be able to edit category's name

# Pre-Conditions: 
System administrator logged in with email (asgardalphaqa@gmail.com) and password (Test1Asgard@)

# Test Steps:  

1. Navigate to https://intense-mesa-78449.herokuapp.com and from navigation bar click on SETTINGS dropdown menu and choose CATEGORIES
2. Select the category you want to update and click on Edit button
3. Click on category's name input field and type new name
4. Click on SAVE button

# Expected Result: 
Updated category's name appears on the list of all categories
 