### Test Case Admin_04

# Title : 
Registered system administrator edit user's post

# Priority: Prio 1

# Test Case Description: 
As a registered system administrator, I would like to be able to edit user's post

# Pre-Conditions: 
System administrator logged in with email (asgardalphaqa@gmail.com) and password (Test1Asgard@)

# Test Steps: 

1. Navigate to https://intense-mesa-78449.herokuapp.com and from navigation bar click on USERS dropdown menu and choose USERS
2. Click on the relevant user's name
3. Click on the relevant post's title
4. Click on Edit link
5. Change the description
6. Click on SAVE button


# Expected Result: 
The description of the post is now updated
 