### Test Case Admin_11

# Title : 
Registered system administrator delete category

# Priority: Prio 2

# Test Case Description: 
As a registered system administrator, I would like to be able to delete category

# Pre-Conditions: 
System administrator logged in with email (asgardalphaqa@gmail.com) and password (Test1Asgard@)

# Steps to reproduce: 

1. Navigate to https://intense-mesa-78449.herokuapp.com and from navigation bar click on SETTINGS dropdown menu and choose CATEGORIES
2. Select the category you want to delete and click on Edit button
3. Click on DELETE button
4. Go to page with message "Are you sure you want to delete category" and current category
5. Click on Yes button

# Expected Result: 
The selected category is not visible anymore in the list of all categories
 