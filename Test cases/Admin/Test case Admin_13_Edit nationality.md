### Test Case Admin_13

# Title : 
Registered system administrator edit nationality

# Priority: Prio 2

# Test Case Description: 
As a registered system administrator, I would like to be able to edit nationality

# Pre-Conditions: 
System administrator logged in with email (asgardalphaqa@gmail.com) and password (Test1Asgard@)

# Test Steps:  

1. Navigate to https://intense-mesa-78449.herokuapp.com and from navigation bar click on SETTINGS dropdown menu and choose NATIONALITIES
2. Click on Edit button of the choosen nationality
3. Click on Nationality's name field and change it
4. Click on SAVE button


# Expected Result: 
The selected nationality appears with the its updated name on the list of all nationalities
 