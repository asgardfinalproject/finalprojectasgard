### Test Case Admin_01

# Title : 
Registered system administrator edit user's first name

# Priority: Prio 1

# Test Case Description: 
As a registered system administrator, I would like to be able to edit user's first name

# Pre-Conditions: 
System administrator logged in with email (asgardalphaqa@gmail.com) and password (Test1Asgard@)

# Test Steps: 

1. Navigate to https://intense-mesa-78449.herokuapp.com and from navigation bar click on USERS dropdown menu and choose USERS
2. Click on the relevant user's name
3. Click on Edit button
4. Click on First name field and type new first name
5. Click on SAVE button

# Expected Result: 
Selected user's name has been updated
 