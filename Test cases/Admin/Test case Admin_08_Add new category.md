### Test Case Admin_08

# Title : 
Registered system administrator add a new category

# Priority: Prio 1

# Test Case Description: 
As a registered system administrator, I would like to be able to add a new category to the list of all categories

# Pre-Conditions: 
System administrator logged in with email (asgardalphaqa@gmail.com) and password (Test1Asgard@)

# Test Steps: 

1. Navigate to https://intense-mesa-78449.herokuapp.com and from navigation bar click on SETTINGS dropdown menu and choose CATEGORIES
2. Click on Add button
3. Click on Choose file from Categoriy's emoticon
4. Choose a new picture from PC and click Open button to upload it
5. Click on input field and type new category name from Categoy's name section
6. Click on SAVE button


# Expected Result: 
New category appears on the bottom of the list of all categories

 