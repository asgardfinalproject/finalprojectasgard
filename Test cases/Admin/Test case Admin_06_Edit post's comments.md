### Test Case Admin_06

# Title : 
Registered system administrator edit post's comment

# Priority: Prio 1

# Test Case Description: 
As a registered system administrator, I would like to be able to edit post's comment

# Pre-Conditions: 
System administrator logged in with email (asgardalphaqa@gmail.com) and password (Test1Asgard@)

# Test Steps: 

1. Navigate to https://intense-mesa-78449.herokuapp.com and from navigation bar click on SETTINGS dropdown menu and choose POSTS
2. Click on one of posts' title
3. Go to Comments section
3. Click on Edit link
4. Click on Comment field and update the current comment
5. Click on SAVE button


# Expected Result: 
The updated comment appears on comments' list of the relevant post
 