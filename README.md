# **Healthy Food Social Network**

## **Documentation:**


[Set Environment](https://intense-mesa-78449.herokuapp.com/)

[Test Plan](https://telerikacademy-my.sharepoint.com/:b:/p/bilyana_popova_a22_learn/Ec4zgxFUD6tDtwVgq7821loB6cOpZ9Qk2wnDFgk0McI6SQ?e=MwVBOk)

[Bilyana Popova's Exploratory Testing Report](https://telerikacademy-my.sharepoint.com/:b:/p/bilyana_popova_a22_learn/Ea6CeIZMHb1GpsdwpYAXMSoBCERNd1XsAyAjslOyWnBEsg?e=1p62sL)

[Sofiya Georgieva's Exploratory Testing Report](https://telerikacademy-my.sharepoint.com/:b:/r/personal/sofiya_georgieva_a22_learn_telerikacademy_com/Documents/SofiyaGeorgieva_ExploratoryTesting.pdf?csf=1&web=1&e=JkK7Q3)

[Test Report](https://telerikacademy-my.sharepoint.com/:b:/p/bilyana_popova_a22_learn/ESiqVfyZSThDvrzZZEYQEFUB-6we6NGw_YFiimAHRH5VSQ?e=UiYtcy)

[Instructions how to run the Tests](https://telerikacademy-my.sharepoint.com/:b:/p/sofiya_georgieva_a22_learn/Ee1FT_nLJiBCnPv-SLxiWfwBcVT18HZ51sTuOvDbJRzqPQ?e=IgA5av)

[Link to issues](https://gitlab.com/asgardfinalproject/finalprojectasgard/-/issues)



