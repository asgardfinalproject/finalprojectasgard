package testCases;
import com.telerikacademy.finalproject.pages.NavigationPage;
import com.telerikacademy.finalproject.pages.UserConnectionPage;
import com.telerikacademy.finalproject.pages.UserProfilePage;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

public class UserConnectRequestTestConfirm extends BaseTest {

    UserProfilePage userPage = new UserProfilePage();
    UserConnectionPage userConnectionPage = new UserConnectionPage();
    NavigationPage navPage = new NavigationPage();

    @Before
    public void authenticate() {
        tastyFoodAPI.authenticateDriverForUser("healthyFoodUserBP.username.encoded",
                "healthyFoodUserBP.pass.encoded", actions.getDriver());
        actions.clickElement(navPage.homeButton);
        actions.getDriver().manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    @Test
    public void confirmConnectionRequest() {

        userConnectionPage.navigateToUserThreeAndConfirmConnection();

        actions.assertElementPresent(userPage.userSaveButton);
        actions.assertElementPresent(navPage.logOutButton);
        actions.clickElement(navPage.logOutButton);
    }
}
