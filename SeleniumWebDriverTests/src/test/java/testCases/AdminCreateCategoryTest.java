package testCases;

import com.telerikacademy.finalproject.pages.AdminCreateCategoryPage;
import com.telerikacademy.finalproject.pages.NavigationPage;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AdminCreateCategoryTest extends BaseTest {

    NavigationPage navPage = new NavigationPage();
    AdminCreateCategoryPage adminPage = new AdminCreateCategoryPage();

    private static final String URL_CATEGORIES = "https://intense-mesa-78449.herokuapp.com/admin/categories";

    @Before
    public void authentication() {
        tastyFoodAPI.authenticateDriverForUser("healthyFoodAdmin.username.encoded",
                "healthyFoodAdmin.pass.encoded", actions.getDriver());
    }

    @Test
    public void adminCreateAndDeleteCategory() {

        adminPage.adminNavigateToCategoryFeed();

        Assert.assertEquals(URL_CATEGORIES, actions.getDriver().getCurrentUrl());

        adminPage.adminCreateNewCategory();
        int sizeBarBefore = actions.size(adminPage.sizeSideBar);

        actions.assertElementPresent(adminPage.nameNewCategory);

        adminPage.adminDeleteNewCategory();
        int sizeBarAfter = actions.size(adminPage.sizeSideBar);

        Assert.assertNotEquals(sizeBarBefore, sizeBarAfter);

        actions.clickElement(navPage.logOutButton);

        actions.isElementPresentUntilTimeout(navPage.login,20);
        actions.assertElementPresent(navPage.login);
    }
}

