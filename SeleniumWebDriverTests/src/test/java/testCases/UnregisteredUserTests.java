package testCases;

import com.telerikacademy.finalproject.pages.NavigationPage;
import com.telerikacademy.finalproject.pages.UnregisteredUserPage;
import org.junit.Assert;
import org.junit.Test;

public class UnregisteredUserTests extends BaseTest {

    UnregisteredUserPage unUserPage = new UnregisteredUserPage();

    private static final String URL_POST_NUMBER_6 = "https://intense-mesa-78449.herokuapp.com/posts/6";
    private static final String URL_POST_NUMBER_88 = "https://intense-mesa-78449.herokuapp.com/posts/88";

    @Test
    public void unregisteredUser_NavigateToCategoryPostFeed() {

        unUserPage.navigateToCurrentCategoryPostFeed();

        actions.assertElementPresent(unUserPage.firstEnergyPostImg);

        actions.clickElement(unUserPage.firstEnergyPost);

        Assert.assertEquals(URL_POST_NUMBER_6, actions.getDriver().getCurrentUrl());
    }


    @Test
    public void unregisteredUser_NavigateToUserProfile() {

        unUserPage.userNavigateToCurrentUserProfile();

        actions.assertElementPresent(unUserPage.userProfileData);
    }

    @Test
    public void unregisteredUser_NavigateToUserPostFeed() {

        unUserPage.navigateToHomePagePost();

        actions.assertElementPresent(unUserPage.homePagePostTitle);

        unUserPage.navigateToCurrentUserPost();

        Assert.assertEquals(URL_POST_NUMBER_88, actions.getDriver().getCurrentUrl());
    }
}

