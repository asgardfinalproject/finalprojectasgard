package testCases;
import com.telerikacademy.finalproject.pages.NavigationPage;
import com.telerikacademy.finalproject.pages.UserCreatePostPage;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

public class UserCreatePostTest extends BaseTest {

    UserCreatePostPage userCreatePostPage = new UserCreatePostPage();
    NavigationPage navPage = new NavigationPage();

    public static final String expectedUrl = "https://intense-mesa-78449.herokuapp.com/users/3";

    @Before
    public void authenticate() {
        tastyFoodAPI.authenticateDriverForUser("healthyFoodUserSG.username.encoded",
                "healthyFoodUserSG.pass.encoded", actions.getDriver());
        actions.clickElement(navPage.homeButton);
        actions.getDriver().manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }
    @Test
    public void createNewPost() {
        navPage.navigateToNewPostButton();
        userCreatePostPage.createNewPublicPost();

        Assert.assertEquals(expectedUrl, actions.getDriver().getCurrentUrl());

        actions.assertElementPresent(navPage.logOutButton);
        actions.clickElement(navPage.logOutButton);
    }



}
