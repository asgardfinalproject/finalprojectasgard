package testCases;

import com.telerikacademy.finalproject.pages.NavigationPage;
import com.telerikacademy.finalproject.pages.UserProfilePage;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

public class UserChangeFirstNameTest extends BaseTest {

    UserProfilePage userPage = new UserProfilePage();
    NavigationPage navPage = new NavigationPage();

    public static final String expectedProfileUpdateUrl = "https://intense-mesa-78449.herokuapp.com/users/3";

    @Before
    public void navigateToHome_UsingNavigation() {
        tastyFoodAPI.authenticateDriverForUser("healthyFoodUserSG.username.encoded",
                "healthyFoodUserSG.pass.encoded", actions.getDriver());
        actions.clickElement(navPage.homeButton);
        actions.getDriver().manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    @Test
    public void updateUserFirstName() {
        userPage.navigateToUserProfile();
        userPage.changeUserFirstName();

        actions.assertElementPresent(userPage.userEdit);

        Assert.assertEquals("Page was not navigated", expectedProfileUpdateUrl,
                actions.getDriver().getCurrentUrl());

        actions.assertElementPresent(navPage.logOutButton);

        actions.clickElement(navPage.logOutButton);
    }
}
