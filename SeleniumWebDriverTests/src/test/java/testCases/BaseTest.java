package testCases;

import com.telerikacademy.finalproject.tastyfoodapi.TastyFoodAPI;
import com.telerikacademy.finalproject.utils.UserActions;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.interactions.Actions;

import java.util.concurrent.TimeUnit;

public class BaseTest {
	UserActions actions = new UserActions();
	Actions act = new Actions(actions.getDriver());
	TastyFoodAPI tastyFoodAPI = new TastyFoodAPI();

	@BeforeClass
	public static void setUp(){
		UserActions.loadBrowser("base.url");
	}

	@AfterClass
	public static void tearDown(){
		UserActions.quitDriver();
	}
}
