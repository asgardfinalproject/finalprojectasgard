package testCases;

import com.telerikacademy.finalproject.pages.NavigationPage;
import com.telerikacademy.finalproject.pages.UserCreateCommentPage;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

public class UserCreateCommentTest extends BaseTest {

    NavigationPage navPage = new NavigationPage();
    UserCreateCommentPage commentPage = new UserCreateCommentPage();

    private static final String urlConnections = "https://intense-mesa-78449.herokuapp.com/connections";
    private static final String urlPostNumber = "https://intense-mesa-78449.herokuapp.com/posts/7";

    @Before
    public void authentication() {
        tastyFoodAPI.authenticateDriverForUser("healthyFoodUserBP.username.encoded",
                "healthyFoodUserBP.pass.encoded", actions.getDriver());
        actions.getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    public void navigateToFriendsFeedCreateAndLikeComment() {

        commentPage.userNavigateToFriendsFeed();

        Assert.assertEquals(urlConnections, actions.getDriver().getCurrentUrl());

        commentPage.userNavigateToFriendsPost();

        Assert.assertEquals(urlPostNumber, actions.getDriver().getCurrentUrl());

        int numberCommentsBefore = actions.size(commentPage.counterComments);
        commentPage.userCreateComment();
        int numberCommentsAfter = actions.size(commentPage.counterComments);

        Assert.assertNotEquals(numberCommentsBefore, numberCommentsAfter);

        actions.clickLastElement(commentPage.likeComment);

        Assert.assertTrue(actions.lastElementIsEnabled(commentPage.dislikeComment));

        actions.clickElement(navPage.logOutButton);

        actions.assertElementPresent(navPage.login);
    }
}




