package testCases;
import com.telerikacademy.finalproject.pages.NavigationPage;
import com.telerikacademy.finalproject.pages.UserConnectionPage;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

public class UserConnectRequestSendTest extends BaseTest {

    UserConnectionPage userConnectionPage = new UserConnectionPage();

    NavigationPage navPage = new NavigationPage();

    @Before
    public void authenticate() {
        tastyFoodAPI.authenticateDriverForUser("healthyFoodUserSG.username.encoded",
                "healthyFoodUserSG.pass.encoded", actions.getDriver());
        actions.clickElement(navPage.homeButton);
        actions.getDriver().manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    @Test
    public void sendConnectionRequest() {

        userConnectionPage.navigateToUserSixAndSendRequest();

        actions.assertElementPresent(userConnectionPage.userRejectButton);
        actions.assertElementPresent(navPage.logOutButton);
        actions.clickElement(navPage.logOutButton);
    }
}