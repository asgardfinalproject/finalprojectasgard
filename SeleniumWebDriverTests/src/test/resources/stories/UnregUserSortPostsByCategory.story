Meta:
@endToEnd

Narrative:
As an unregistered user
I want to be able to see all public posts and categories
So that I can sort by category

Scenario: Unregistered user see all public posts and sort by category
Given Click navigation.LatestPosts element
When Wait navigation.CategoryEnergy for 30 seconds
And Click navigation.CategoryEnergy element
And Wait unUserBrowsing.FirstEnergyPost for 30 seconds
Then Click unUserBrowsing.FirstEnergyPost element

Scenario: Unregistered user see current user profile
Given Click navigation.Users element
When Wait unUserBrowsing.UserIdFive for 20 seconds
Then Click unUserBrowsing.UserIdFive element

Scenario: Unregistered user choose a post from Home page and see creator's profile
Given Click navigation.Home element
When Wait unUserBrowsing.HomePagePost for 20 seconds
And Click unUserBrowsing.HomePagePost element
And Click unUserBrowsing.UserInfo element
And Wait unUserBrowsing.LastUserPost for 20 seconds
Then Click unUserBrowsing.LastUserPost element