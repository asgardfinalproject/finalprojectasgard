package com.telerikacademy.finalproject.pages;

public class UnregisteredUserPage extends BasePage {

    NavigationPage navPage = new NavigationPage();

    public UnregisteredUserPage() {
        super("postSix.url");
    }

    public final String homePagePostButton = "unUserBrowsing.HomePagePost";
    public final String userInfo = "unUserBrowsing.UserInfo";
    public final String lastUserPost = "unUserBrowsing.LastUserPost";
    public final String firstEnergyPost = "unUserBrowsing.FirstEnergyPost";
    public final String userIdFive = "unUserBrowsing.UserIdFive";
    public final String homePagePostTitle = "unUserBrowsing.HomePagePostTitle";
    public final String userProfileData = "unUserBrowsing.UserProfileData";
    public final String firstEnergyPostImg = "unUserBrowsing.FirstEnergyPostImg";


    public void navigateToCurrentCategoryPostFeed() {
        actions.clickElement(navPage.latestPostsButton);
        actions.isElementPresentUntilTimeout(navPage.categoryEnergyButton, 20);
        actions.clickElement(navPage.categoryEnergyButton);
        actions.isElementPresentUntilTimeout(firstEnergyPostImg, 30);
    }

    public void userNavigateToCurrentUserProfile() {
        actions.clickElement(navPage.usersButton);
        actions.isElementPresentUntilTimeout(userIdFive, 20);
        actions.clickElement(userIdFive);
    }

    public void navigateToCurrentUserPost() {
        actions.clickElement(userInfo);
        actions.isElementPresentUntilTimeout(lastUserPost, 20);
        actions.clickElement(lastUserPost);
    }

    public void navigateToHomePagePost() {
        actions.clickElement(navPage.homeButton);
        actions.isElementPresentUntilTimeout(homePagePostButton, 30);
        actions.clickElement(homePagePostButton);
    }

}
