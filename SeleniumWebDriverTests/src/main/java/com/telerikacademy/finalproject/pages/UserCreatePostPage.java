package com.telerikacademy.finalproject.pages;

public class UserCreatePostPage extends BasePage {

    public UserCreatePostPage() {
        super("base.url");
    }

    AdminCreateCategoryPage adminPage = new AdminCreateCategoryPage();

//    private static final String POST_PATH_IMG = "C:\\FinalProject\\healthy-food-social-network\\images\\smoothies.png";
    private static final String POST_TITLE_VALUE = "Two-minute breakfast smoothie";
    private static final String POST_DESCRIPTION_VALUE = "Put all the ingredients in a blender and whizz for 1 min until smooth. Pour the banana oat smoothie into two glasses to serve.";

    public final String postVisibilityIdPublic = "userCreatingPost.PostVisibilityIdPublic";
    public final String postDescriptionElement = "userCreatingPost.Description";
    public final String postTitleElement = "userCreatingPost.Title";

    public void createNewPublicPost() {

        actions.clickElement(postVisibilityIdPublic);
        actions.uploadImage(getImagePath(), adminPage.fileButton);
        actions.clickElement(postTitleElement);
        actions.typeValueInField(POST_TITLE_VALUE,postTitleElement);

        actions.clickElement(postDescriptionElement);
        actions.typeValueInField(POST_DESCRIPTION_VALUE, postDescriptionElement);
        actions.clickElement(adminPage.saveButton);
    }

    public String getImagePath() {

        return System.getProperty("user.dir") + "\\src\\test\\resources\\smoothies.png";
    }
}
