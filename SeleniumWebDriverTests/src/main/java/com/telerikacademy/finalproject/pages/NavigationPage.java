package com.telerikacademy.finalproject.pages;

public class NavigationPage extends BasePage {
    public NavigationPage() {

        super("base.url");
    }

    public final String homeButton = "navigation.Home";
    public final String logOutButton = "navigation.LogOut";
    public final String latestPostsButton = "navigation.LatestPosts";
    public final String categoryEnergyButton = "navigation.CategoryEnergy";
    public final String usersButton = "navigation.Users";
    public final String profileButton = "navigation.Profile";
    public final String requestsButton = "navigation.Requests";
    public final String friendsButton = "navigation.Friends";
    public final String settings="navigation.Settings";
    public final String categories="navigation.Categories";
    public final String headerArea = "navigation.HeaderArea";
    public final String profileEditLink = "navigation.ProfileEditLink";
    public final String recipesHome ="navigation.RecipesHome";
    public final String allUsersLink = "navigation.UsersAllUsersLink";
    public final String newPostButton = "navigation.NewPost";
    public final String bell = "navigation.Bell";
    public final String login="navigation.Login";

    public void navigateToNewPostButton() {
        actions.isElementPresentUntilTimeout(latestPostsButton, 20);
        actions.moveToElement(latestPostsButton);

        actions.isElementPresentUntilTimeout(newPostButton, 20);
        actions.moveToElement(newPostButton);
        actions.clickElement(newPostButton);
    }
}