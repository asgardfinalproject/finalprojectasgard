package com.telerikacademy.finalproject.pages;

public class AdminCreateCategoryPage extends BasePage {

    NavigationPage navPage = new NavigationPage();

    private static final String CATEGORY = "Spices";
//    private static final String PATH_IMAGE = "D:\\QA\\FINAL PROJECT\\healthy-food-social-network\\images\\spices.png";


    public AdminCreateCategoryPage() {

        super("base.url");
    }

    public final String addCategoryButton = "adminCreatingCategory.AddButton";
    public final String CategoryPlaceholder = "adminCreatingCategory.CategoryPlaceholder";
    public final String fileButton = "adminCreatingCategory.FileButton";
    public final String saveButton = "adminCreatingCategory.SaveCategory";
    public final String nameNewCategory = "adminCreatingCategory.NameNewCategory";
    public final String editCategory = "adminCreatingCategory.EditCategory";
    public final String deleteCategory = "adminCreatingCategory.DeleteCategory";
    public final String sizeSideBar = "adminCreatingCategory.SizeSideBar";


    public void adminNavigateToCategoryFeed() {
        actions.clickElement(navPage.homeButton);
        actions.moveToElement(navPage.settings);
        actions.moveToElement(navPage.settings);
        actions.clickElement(navPage.categories);
    }

    public void adminCreateNewCategory() {
        actions.isElementPresentUntilTimeout(addCategoryButton, 20);
        actions.clickElement(addCategoryButton);
        actions.isElementPresentUntilTimeout(CategoryPlaceholder, 20);
        actions.clickElement(CategoryPlaceholder);
        actions.isElementPresentUntilTimeout(CategoryPlaceholder, 20);
        actions.typeValueInField(CATEGORY, CategoryPlaceholder);
        actions.uploadImage(getImagePath(), fileButton);
        actions.clickElement(saveButton);
    }

    public void adminDeleteNewCategory() {
        actions.isElementPresentUntilTimeout(editCategory, 30);
        actions.clickElement(editCategory);
        actions.isElementPresentUntilTimeout(deleteCategory, 20);
        actions.clickElement(deleteCategory);
        actions.isElementPresentUntilTimeout(saveButton, 20);
        actions.clickElement(saveButton);
    }

    public String getImagePath() {
        return System.getProperty("user.dir") + "\\src\\test\\resources\\spices.png";
    }

}
