package com.telerikacademy.finalproject.pages;

public class UserProfilePage extends BasePage {
    public UserProfilePage() {

        super("base.url");
    }

    private static final String OLD_PASSWORD = "Univercity1@";
    private static final String NEW_PASSWORD = "Univercity1#";

    NavigationPage navPage = new NavigationPage();

    public final String userEdit= "user.Edit";
    public final String userFirstName = "user.FirstName";
    public final String userSaveButton = "user.SaveButton";
    public final String userChangePassword = "user.ChangePassword";
    public final String userOldPassword = "user.OldPassword";
    public final String userNewPassword = "user.NewPassword";
    public final String userPasswordConfirmation ="user.PasswordConfirmation";
    public final String userPasswordSave = "user.PasswordSave";

    public void changeUserFirstName() {
        actions.clickElement(userFirstName);
        actions.clear(userFirstName);
        actions.typeValueInField("Sof", userFirstName);
        actions.clickElement(userSaveButton);
    }

    public void navigateToUserProfile() {
        actions.moveToElement(navPage.profileButton);
        actions.moveToElement(navPage.profileEditLink);
        actions.clickElement(navPage.profileEditLink);
    }

    public void passwordReset() {
        actions.clickElement(userChangePassword);
        actions.clickElement(userOldPassword);
        actions.typeValueInField(OLD_PASSWORD, userOldPassword);
        actions.clickElement(userNewPassword);
        actions.typeValueInField(NEW_PASSWORD, userNewPassword);
        actions.clickElement(userPasswordConfirmation);
        actions.typeValueInField(NEW_PASSWORD, userPasswordConfirmation);
        actions.clickElement(userPasswordSave);
    }
}
