package com.telerikacademy.finalproject.pages;

public class UserConnectionPage extends BasePage {

    NavigationPage navPage = new NavigationPage();

    public UserConnectionPage() {

        super("base.url");
    }
    public final String userConnectButton = "user.ConnectButton";
    public final String userUserIdThree = "user.UserIdThree";
    public final String userConfirmButton = "user.ConfirmButton";
    public final String userRejectButton = "user.RejectButton";
    public final String userUserIdSix = "user.UserIdSix";

    public void navigateToUserThreeAndConfirmConnection() {
        actions.clickElement(navPage.homeButton);
        actions.moveToElement(navPage.usersButton);
        actions.moveToElement(navPage.requestsButton);
        actions.isDisplayed(navPage.bell);
        actions.clickElement(navPage.requestsButton);
        actions.clickElement(userUserIdThree);
        actions.clickElement(userConfirmButton);
    }

    public void navigateToUserSixAndSendRequest() {
        actions.clickElement(navPage.homeButton);
        actions.moveToElement(navPage.usersButton);
        actions.moveToElement(navPage.allUsersLink);
        actions.clickElement(navPage.allUsersLink);
        actions.clickElement(userUserIdSix);
        actions.clickElement(userConnectButton);
    }
}
