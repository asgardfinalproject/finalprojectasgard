package com.telerikacademy.finalproject.pages;

public class UserCreateCommentPage extends BasePage {

    NavigationPage navPage = new NavigationPage();

    private static final String COMMENT = "I love Kitesurfing";

    public UserCreateCommentPage() {

        super("base.url");
    }

    public final String userIdFive = "userCreatingComment.UserIdFive";
    public final String commentArea="userCreatingComment.CommentArea";
    public final String saveCommentButton="userCreatingComment.SaveCommentButton";
    public final String userPost="userCreatingComment.UserPost";
    public final String counterComments="userCreatingComment.CounterComments";
    public final String likeComment="userCreatingComment.UserLikeComment";
    public final String dislikeComment="userCreatingComment.UserDislikeComment";

    public void userNavigateToFriendsFeed() {
        actions.clickElement(navPage.homeButton);
        actions.moveToElement(navPage.usersButton);
        actions.moveToElement(navPage.friendsButton);
        actions.clickElement(navPage.friendsButton);
    }

    public void userNavigateToFriendsPost() {
        actions.clickElement(userIdFive);
        actions.clickElement(userPost);
    }

    public void userCreateComment() {
        actions.clickElement(commentArea);
        actions.typeValueInField(COMMENT, commentArea);
        actions.clickElement(saveCommentButton);
    }
}
