package com.telerikacademy.finalproject.utils;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class UserActions {
    public WebDriver getDriver() {
        return driver;
    }

    final WebDriver driver;
    final Actions actions;

    public UserActions() {
        this.actions = new Actions(Utils.getWebDriver());
        this.driver = Utils.getWebDriver();
    }

    public static void loadBrowser(String baseUrlKey) {
        Utils.getWebDriver().get(Utils.getConfigPropertyByKey(baseUrlKey));
    }

    public static void quitDriver() {
        Utils.tearDownWebDriver();
    }

    //############# ELEMENT OPERATIONS #########

    public void clickElement(String key, Object... arguments) {
        String locator = Utils.getUIMappingByKey(key, arguments);
        Utils.LOG.info("Clicking on element " + key);
        WebElement element = driver.findElement(By.xpath(locator));
        element.click();
    }

    public void typeValueInField(String value, String field, Object... fieldArguments) {
        String locator = Utils.getUIMappingByKey(field, fieldArguments);
        WebElement element = driver.findElement(By.xpath(locator));
        element.sendKeys(value);
    }

    public void switchToIFrame(String iframe) {
        WebElement iFrame = driver.findElement(By.xpath(Utils.getUIMappingByKey(iframe)));
        getDriver().switchTo().frame(iFrame);
    }

    public void clear(String key, Object... arguments) {
        String locator = Utils.getUIMappingByKey(key, arguments);
        WebElement element = driver.findElement(By.xpath(locator));
        element.clear();
    }

    public int size(String key, Object... arguments) {
        String locator = Utils.getUIMappingByKey(key, arguments);
        List<WebElement> element = driver.findElements(By.xpath(locator));
        return element.size();
    }

    public void clickLastElement(String key, Object... arguments) {
        String locator = Utils.getUIMappingByKey(key, arguments);
        List<WebElement> element = driver.findElements(By.xpath(locator));
        WebElement lastElement = element.get((element.size() - 1));
        lastElement.click();
    }

    public boolean lastElementIsEnabled(String key, Object... arguments) {
        String locator = Utils.getUIMappingByKey(key, arguments);
        List<WebElement> element = driver.findElements(By.xpath(locator));
        WebElement lastElement = element.get((element.size() - 1));
        return lastElement.isEnabled();
    }

    public boolean text(String key, Object... arguments) {
        String locator = Utils.getUIMappingByKey(key, arguments);
        WebElement element = driver.findElement(By.xpath(locator));
        element.getText();
        return true;
    }

    public boolean isDisplayed(String key, Object... arguments) {
        String locator = Utils.getUIMappingByKey(key, arguments);
        WebElement element = driver.findElement(By.xpath(locator));
        return element.isDisplayed();

    }

    public void uploadImage(String path, String key, Object... arguments) {
        String locator = Utils.getUIMappingByKey(key, arguments);
        WebElement element = driver.findElement(By.xpath(locator));
        String pathImage = Utils.getUIMappingByKey(path);
//        String pathWeb = System.getProperty("user.dir")+;
        element.sendKeys(pathImage);
    }

    public void moveToElement(String key, Object... arguments) {
        String locator = Utils.getUIMappingByKey(key, arguments);
        Utils.LOG.info("Moving on element " + key);
        WebElement element = driver.findElement(By.xpath(locator));
        actions.moveToElement(element).build().perform();
    }

    public String getImagePath() {
       return System.getProperty("user.dir")+"src\\test\\resources\\spices.png";
    }

    //############# WAITS #########

    public boolean isElementPresentUntilTimeout(String locator, int timeout, Object... arguments) {
        try {
            String xpath = Utils.getUIMappingByKey(locator, arguments);
            Integer defaultTimeout = Integer.parseInt(Utils.getConfigPropertyByKey("config.defaultTimeoutSeconds"));
            WebDriverWait wait = new WebDriverWait(driver, timeout);
            wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(xpath)));
            return true;
        } catch (Exception exception) {
            return false;
        }
    }

    //############# ASSERTS #########

    public void assertElementPresent(String locator) {
        Assert.assertNotNull(driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))));
    }
}
